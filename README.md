# Nginx server header removal

Nginx server header removal patch in a variety of flavors:
- Nginx 1.22 stable
- Nginx 1.24 stable
- Nginx 1.26 stable with HTTP/3
- Nginx 1.27 mainline with HTTP/3

---

Showing which web server software you currently use, is almost asking for problems, in the case of a targeted attack.

As written in the IETF internet standard RFC 2616.
Your web server comply with the internet standard if your web server wont display the specific, used software.

Because it is not a hard requirement to show, it is a choice.
Not a must have as many software developers would like you to believe.

This patch will remove Nginx as server header.
Which means the server will no longer send the software used in the header.

* Internet Engineering Task Force (IETF)
